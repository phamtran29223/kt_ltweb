﻿using System.ComponentModel.DataAnnotations;

namespace KT_ST5.Models
{
    public class Logs
    {
        [Key]
        public int LogID { get; set; }
        public int TransactionID { get; set; }
        public Transactions? Transaction { get; set; }

        public DateTime? loginDate { get; set; }
   
        public string LoginTime { get; set; }

    }
}
