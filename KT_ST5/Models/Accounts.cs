﻿using System.ComponentModel.DataAnnotations;

namespace KT_ST5.Models
{
    public class Accounts
    {
        [Key]
        public int AccountID { get; set; }
      
        public int CustomerID { get; set; }
        public Customer? Customer { get; set; }
        public string AccountName { get; set; }
        
   
    }
}
