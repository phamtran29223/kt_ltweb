﻿using System.ComponentModel.DataAnnotations;

namespace KT_ST5.Models
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAndAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
