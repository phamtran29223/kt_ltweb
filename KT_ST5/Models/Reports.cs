﻿using System.ComponentModel.DataAnnotations;

namespace KT_ST5.Models
{
    public class Reports
    {
        [Key]
        public int ReportID { get; set; }
        public int AccountID { get; set; }
        public Accounts? Account { get; set; }
        public int LogsID { get; set; }
        public Logs? Logs { get; set; }
        public int TransactionalID { get; set; }

        public Transactions? Transaction { get; set; }  
        public string ReportName { get; set; }
        public DateTime? ReportDate { get; set; }
        
    }
}
