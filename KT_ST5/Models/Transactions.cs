﻿using System.ComponentModel.DataAnnotations;

namespace KT_ST5.Models
{
    public class Transactions { 
    [Key]
        public int TransactionalID { get; set; }
        public int EmployeeID { get; set; }
        public Employees? Employee { get; set; }
        public int CustomID { get; set; }
        public Customer? Customer { get; set; }
        public string Name { get; set; }
       
      
    }
}
